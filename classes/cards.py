"""
https://docs.python.org/3/reference/datamodel.html
"""
import random

class Deck:
    def __init__(self):
        self.cards = list()
        for rank in ["6", "7", "8", "9", "10", "J", "D", "K", "A"]:
            for suit in ["D", "C", "H", "S"]:
                self.cards.append(Card(suit, rank))

    def shuffle(self):
        random.shuffle(self.cards)

    def draw(self):
        return self.cards.pop()

    @property
    def empty(self):
        return len(self.cards) <= 0

class Hand:
    def __init__(self):
        self.cards = list()

    def __str__(self):
        return " ".join([f"[{el}]" for el in self.cards])

    def add_top(self, card):
        self.cards.append(card)

    def add_bottom(self, card):
        if isinstance(card, Card):
            self.cards.insert(0, card)
        elif isinstance(card, list):
            self.cards = card + self.cards

    def draw(self):
        return self.cards.pop()

    @property
    def empty(self):
        return len(self.cards) <= 0

    @property
    def nmb_cards(self):
        return len(self.cards)

class Card:
    def __init__(self, suit, rank):
        self.suit = suit
        self.rank = rank

        self.weights = {"6" : 6,
                        "7" : 7,
                        "8" : 8,
                        "9" : 9,
                        "10": 10,
                        "J" : 14,
                        "D" : 11,
                        "K" : 12,
                        "A" : 13
                        }

    def __str__(self):
        return f"{self.suit}{self.rank}"

    def __lt__(self, other):
        return self.weight < other.weight

    def __gt__(self, other):
        return self.weight > other.weight

    def __eq__(self, other):
        return self.weight == other.weight

    @property
    def weight(self):
        return self.weights[self.rank]
