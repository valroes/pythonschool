"""
https://en.wikipedia.org/wiki/Object_composition
https://docs.python.org/3/reference/datamodel.html
"""
import os
import time
import cards

class Player:
    def __init__(self, name):
        self.hand = cards.Hand()
        self.name = name

    def form_deckbar(self):
        return "|" * self.hand.nmb_cards

class Burechrieg:
    def __init__(self):
        self.deck    = cards.Deck()
        self.player1 = Player("Valentin")
        self.player2 = Player("Marius")

        self.winner = None

    def main(self):
        self.setup()
        while not self.winner:
            self.trick()
            time.sleep(0.1)
            os.system("clear")

        print(f"The winner is {self.winner.name}!")


    def setup(self):
        self.deck.shuffle()
        while not self.deck.empty:
            self.player1.hand.add_top(self.deck.draw())
            self.player2.hand.add_top(self.deck.draw())

    def trick(self, stake=list()):
        if self.check_winner():
            return

        card1 = self.player1.hand.draw()
        card2 = self.player2.hand.draw()

        print(self.player1.name, self.player1.form_deckbar())
        print(card1)
        print()
        print(card2)
        print(self.player2.name, self.player2.form_deckbar())

        if   card1 > card2:
            self.player1.hand.add_bottom(stake+[card1, card2])
        elif card1 < card2:
            self.player2.hand.add_bottom(stake+[card1, card2])
        else:
            if self.check_winner():
                return

            stake = [card1, card2, self.player1.hand.draw(), self.player2.hand.draw()]
            print(f"It's a draw! Stake: {' '.join([str(el) for el in stake])}")
            self.trick(stake)

    def check_winner(self):
        if self.player1.hand.empty:
            self.winner = self.player2
            return True
        elif self.player2.hand.empty:
            self.winner = self.player1
            return True
        else:
            return False


if __name__ == "__main__":
    Burechrieg().main()





